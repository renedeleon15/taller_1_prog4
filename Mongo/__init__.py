from pymongo import MongoClient
from InsertData import User
from random import randint



print("*** PROGRAMA PARA INSERTAR REGISTROS EN LA TABLA TEST ***")

usernames = ['Boomer', 'Boss', 'Budweiser', 'Master', 'Meatball', 'Kraken', 'Speed', 'Tornado', 'Spike', 'Doctor']
realnames = ['ANTONIO', 'MARIA', 'JOSE', 'JOSEFA', 'JUAN', 'ISABEL', 'MANUEL', 'CARMEN', 'JESUS', 'FRANCISCA']
realsurnames = ['GARCIA', 'MARTINEZ', 'LOPEZ', 'SANCHEZ', 'GONZALEZ', 'GOMEZ', 'FERNANDEZ', 'MORENO', 'JIMENEZ', 'PEREZ']

for x in range(1, 1001):
    # Crear una lista de objetos para User
    users = [
        User(
            randint(1, 100000000),
            usernames[randint(0, (len(usernames)) - 1)],
            realnames[randint(0, (len(realnames)) - 1)],
            realsurnames[randint(0, (len(realsurnames)) - 1)],
            randint(18, 80)
            )
    ]

    #Conectar con el server de mongoDB pasando host y puerto
    mongoClient = MongoClient('localhost', 27017)

    #Conexión a la base de datos
    db = mongoClient.Accounts

    #Obtener colección
    collection = db.Users

    #Meter los objetos User en la colección user
    for user in users:
        collection.insert_one(user.toDBCollection())

    #conteo de registros
    print(F"Creado y guardado registro {x} / 1000000 como {user.id}")

print("El proceso se ha culminado ¡EXITOSAMENTE!...")



