
class User:

    def __init__(self, id, username, name, surname, age):
        self.id = id
        self.username = username
        self.name = name
        self.surname = surname
        self.age = age

    def toDBCollection(self):
        return {
            "id": self.id,
            "username": self.username,
            "name": self.name,
            "surname": self.surname,
            "age": self.age
        }

    def __str__(self):
        return "- User: %s - ID: %i - Nombre: %s %s - Edad: %i" \
               %(self.username, self.id, self.name, self.surname, self.age)






