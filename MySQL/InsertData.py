import sqlite3
from random import randint

print("*** PROGRAMA PARA INSERTAR REGISTROS EN LA TABLA TEST ***")

usernames = ['Boomer', 'Boss', 'Budweiser', 'Master', 'Meatball', 'Kraken', 'Speed', 'Tornado', 'Spike', 'Doctor']
realnames = ['ANTONIO', 'MARIA', 'JOSE', 'JOSEFA', 'JUAN', 'ISABEL', 'MANUEL', 'CARMEN', 'JESUS', 'FRANCISCA']
realsurnames = ['GARCIA', 'MARTINEZ', 'LOPEZ', 'SANCHEZ', 'GONZALEZ', 'GOMEZ', 'FERNANDEZ', 'MORENO', 'JIMENEZ', 'PEREZ']

for x in range(1, 1001):

    id = randint(10000000, 100000000)
    username = usernames[randint(0, (len(usernames))-1)]
    realname = realnames[randint(0, (len(realnames))-1)]
    realsurname = realsurnames[randint(0, (len(realsurnames)) - 1)]
    age = randint(18, 65)

    # Establecer la conexion
    conexion = sqlite3.connect("sqlite3/Test.db")

    # Selecciona el cursor para iniciar la consulta
    consulta = conexion.cursor()

    # Valor de los argumentos
    argumentos = (id, username, realname, realsurname, age)

    sql = """
    INSERT INTO Test(id, username, realname, realsurname, age)
    VALUES (?, ?, ?, ?, ?)
    """

    if (consulta.execute(sql, argumentos)):
        print(F"Creado y guardado registro N° {x} / 1000000 como {id}")
    else:
        print("Ha ocurrido un error al guardar el registro...")
        # Cerrar Conexión
        conexion.close()

    # Terminar consulta
    consulta.close()

    # Guardar cambios en DB
    conexion.commit()



print("El proceso se ha culminado ¡EXITOSAMENTE!...")

#Cerrar Conexión
conexion.close()

