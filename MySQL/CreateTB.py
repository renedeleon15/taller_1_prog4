import sqlite3

#Conectar a la base de datos
conexion = sqlite3.connect("sqlite3/Test.db")

#Seleccionar el cursor
consulta = conexion.cursor()

#SQL code
sql = """
CREATE TABLE IF NOT EXISTS Test(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
username VARCHAR(50) NOT NULL,
realname VARCHAR(50) NOT NULL,
realsurname VARCHAR(50) NOT NULL,
age INTEGER NOT NULL)
"""

#Execute consult
if(consulta.execute(sql)): print("Tabla creada con éxito")
else: print("Ha ocurrido un error al crear la tabla")

#Finish
consulta.close()

#Save changes
conexion.commit()

#Close conection
conexion.close()
